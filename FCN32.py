#!/usr/bin/env python3
#Author Max Schumacher
import numpy as np
import torch
import torch.nn as nn
import torch.optim as optim
import skimage.io as io
from torch.nn.init import xavier_normal_, normal_
import matplotlib.pyplot as plt
from distutils.version import LooseVersion
import torch.nn.functional as F
from torch.utils.data import Dataset, DataLoader
from dataset import *
import scipy.misc as misc


try:
    from tqdm import tqdm, trange
    print_fn = tqdm.write
    has_tqdm = True
except ImportError:
    print_fn = print
    has_tqdm = False

def set_weights(module):
    '''
    Setting random weights and bias for a given module (normal distribution).
    '''
    if isinstance(module, nn.Conv2d):
        xavier_normal_(module.weight)
        normal_(module.bias)
    elif isinstance(module, nn.Linear):
        normal_(module.weight)
        normal_(module.bias)

n_class    = 2
lr         = 1e-4
momentum   = 0
w_decay    = 1e-5
step_size  = 50
gamma      = 0.5

class FCN32s(nn.Module):
    '''
    Input: binary Image of arbitrary size
    Output: Semantic segmentation of the image
    '''
    def __init__(self, n_class):
        super(FCN32s, self).__init__()

        # conv1
        self.conv1 = nn.Sequential(nn.Conv2d(1, 64, 3, padding=100),
                                   nn.ReLU(inplace=True),
                                   nn.MaxPool2d(2, stride=2, ceil_mode=True))  # 1/2

        # conv2
        self.conv2 = nn.Sequential(nn.Conv2d(64, 128, 3, padding=100),
                                   nn.ReLU(inplace=True),
                                   nn.MaxPool2d(2, stride=2, ceil_mode=True))  # 1/4


        # conv3
        self.conv3 = nn.Sequential(nn.Conv2d(128, 256, 3, padding=1),
                                   nn.ReLU(inplace=True),
                                   nn.MaxPool2d(2, stride=2, ceil_mode=True))  # 1/8

        # conv4
        self.conv4 = nn.Sequential(nn.Conv2d(256, 512, 3, padding=1),
                                   nn.ReLU(inplace=True),
                                   nn.MaxPool2d(2, stride=2, ceil_mode=True))  # 1/16

        # conv5
        self.conv5 = nn.Sequential(nn.Conv2d(512, 512, 3, padding=1),
                                   nn.ReLU(inplace=True),
                                   nn.MaxPool2d(2, stride=2, ceil_mode=True))  # 1/32

        # fc6
        self.fc6 = nn.Sequential(nn.Conv2d(512, 4096, 7),
                                 nn.ReLU(inplace=True),
                                 nn.Dropout2d())

         # fc7
        self.fc7 = nn.Sequential(nn.Conv2d(4096, 4096, 1),
                                 nn.ReLU(inplace=True),
                                 nn.Dropout2d())

        self.score_fr = nn.Conv2d(4096, n_class, 1)
        self.upscore = nn.ConvTranspose2d(n_class, n_class, 64, stride=32,
                                          bias=False)

        self.apply(set_weights)

    def forward(self, x):
        h = x
        h = self.conv1(h)
        h = self.conv2(h)
        h = self.conv3(h)
        h = self.conv4(h)
        h = self.conv5(h)
        h = self.fc6(h)
        h = self.fc7(h)
        h = self.score_fr(h)
        h = self.upscore(h)
        h = h[:, :, 19:19 + x.size()[2], 19:19 + x.size()[3]].contiguous()

        return h


def train_model(model, dataloader, use_gpu, n_epochs=121):

    acc_ = np.array([])
    loss_ = np.array([])
    if use_gpu:
        model.cuda()

    # loss and optimizer
    Loss = nn.CrossEntropyLoss()
    Optimizer = optim.RMSprop(model.parameters(), lr=lr, momentum=momentum, weight_decay=w_decay)

    Optimizer.zero_grad()

    if has_tqdm:
        e_iterator = trange(n_epochs, desc='epoch')
    else:
        e_iterator = range(n_epochs)

    for e in e_iterator:
        if has_tqdm:
            iterator = enumerate(tqdm(dataloader, desc='batch'))
        else:
            iterator = enumerate(dataloader)

        for e_step, (x, y) in iterator:
            train_step = e_step + len(dataloader)*e

            if use_gpu:
                x = x.cuda()
                y = y.cuda()

            data = x
            x = x.view((1, 1, 286, 384))

            # Forward
            prediction = model(x)

            # loss
            loss = Loss(prediction, y)

            Optimizer.zero_grad()

            # Backward
            loss.backward()

            # Update
            Optimizer.step()

            # metrics
            pred1 = prediction[0][0].cpu().detach().numpy()
            pred2 = prediction[0][1].cpu().detach().numpy()
            pred = np.greater(pred2, pred1).astype(int).flatten()
            true = y.cpu().detach().numpy().flatten()
            acc = np.mean(np.array([1 if p == t else 0 for p,t in zip(pred, true)]))

            if train_step % 50 == 0:
                #print('predict:',lbl_pred)
                print_fn('{}: Batch-Accuracy = {}, Loss = {}'
                         .format(train_step, acc, float(loss)))
                acc_ = np.append(acc_, acc)
                loss_ = np.append(loss_, float(loss))

        if(e%10 == 0):
            torch.save(model.state_dict(), '{}-{}.ckpt'.format('data_final/checkpoints2/training', e))

    np.save('data_final/acc.npy', acc_)
    np.save('data_final/loss.npy', loss_)

if __name__ == '__main__':
    pass
