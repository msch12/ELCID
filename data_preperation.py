#!/usr/bin/env python3
#Author Michael Anders
import glob
import numpy as np
import scipy.misc as misc
import matplotlib.pyplot as plt
from face_recognition import face_locations
from blob_detection import dogInterestPoints, accuracy
import random
import math
from sklearn.model_selection import train_test_split
import cv2

def load_filelist(folder='data/BioID', type='pgm'):
    ''' load filelist:
                    Args: folder = Location of the data
                          type   = the type of the data we're searching for
    '''
    filelist = glob.glob(folder+'/*.'+type)
    return sorted(filelist)


def load_Images(filelist):
    ''' loads the images of a given filelist into an numpy array '''
    images = np.array([misc.imread(file) for file in filelist])
    return images


def load_eye_position(filelist):
    ''' loads the images of a given filelist into an numpy array '''
    # remove \n and parse at \t, discard first line since it doesnt contain
    # useful information
    lines = [open(file).readlines()[1].rstrip() for file in filelist]
    lines = np.array([line.split("\t") for line in lines])
    return lines.astype(int)


def get_face_loc(images):
    ''' for N given images get_face_loc calculates corner
    coordinates of the face
    '''
    face_loc = [face_locations(image) for image in images]
    if len(face_loc[0])==0:
        face_loc = None
    return face_loc


def remove_negatives(images, face_loc, eyepos):
    '''removes samples for which no face was detected'''
    idx = np.array([1 if len(loc) == 1 else 0 for loc in face_loc])
    idx = np.where(idx == 1)[0]
    images_ = images[idx]
    face_loc_ = np.array(face_loc)[idx]
    eyes = np.array(eyepos)[idx]
    return images_, face_loc_, eyes


def crop_images(images, face_loc):
    '''crops the face part out of the image'''
    cropped = [img[loc[0][0]:loc[0][2], loc[0][3]:loc[0][1]]
               for img, loc in zip(images, face_loc)]
    return cropped


def crop_bounding_box(image, blobs):
    ''' crop a bounding box around the found interest points '''

    cropped = [image[max(0, blob[1]-12):blob[1]+12, max(0,blob[0]-25):blob[0]+25]
               for blob in blobs]
    cropped = [pad(img) for img in cropped]
    return cropped

def pad(img):
    '''Pad the image to fit into the 24x50 scale used by the CNN'''
    height, width = np.shape(img)[0], np.shape(img)[1]


    if (height<24 or width < 50):

        dTop = int((24-height+(24-height) % 2)/2)
        dBot = int((24-height-(24-height) % 2)/2)
        dLeft = int((50-width+(50-width) % 2)/2)
        dRight = int((50-width-(50-width) % 2)/2)

        return cv2.copyMakeBorder(img, dTop, dBot, dLeft, dRight,
                              cv2.BORDER_REFLECT)
    else:
        return img
def create_eye_data(images, eye_positions, size, folder):
    ''' creates the dataset of eye-samples '''
    for i in range(size):
        # coordinates lef/right eye of sample i
        left_coord = [eye_positions[i][0], eye_positions[i][1]]
        right_coord = [eye_positions[i][2], eye_positions[i][3]]
        cropped = crop_bounding_box(images[i], [left_coord, right_coord])

        # save the images for left and right eye position
        misc.imsave(folder + "/Cropped_Eyes/BioID_"+str(i).zfill(4)
                    + "_le.pgm", cropped[0])
        misc.imsave(folder + "/Cropped_Eyes/BioID_"+str(i).zfill(4)
                    + "_re.pgm", cropped[1])

def create_shifted_eye_data(images,eye_positions,size,folder):
    ''' creates a dataset of eye-samples shifted by random values'''
    for k in range(2):
        for i in range(size):
            left_coord = [eye_positions[i][0], eye_positions[i][1]]
            right_coord = [eye_positions[i][2], eye_positions[i][3]]
            rand1=random.randint(-10,10)
            rand2=random.randint(-7,7)
            rand3=random.randint(-10,10)
            rand4=random.randint(-7,7)
            left_coord_shifted=[left_coord[0]+rand1,left_coord[1]+rand2]
            right_coord_shifted = [right_coord[0]+rand3,right_coord[1]+rand4]
            cropped= crop_bounding_box(images[i],[left_coord_shifted,right_coord_shifted])
            misc.imsave(folder + "/Cropped_Eyes_Shifted/BioID_"+str(i).zfill(4)
                        + "_le_"+str(k)+".pgm", cropped[0])
            misc.imsave(folder + "/Cropped_Eyes_Shifted/BioID_"+str(i).zfill(4)
                        + "_re"+str(k)+".pgm", cropped[1])

def create_negative_eye_data(images, face_loc, eye_positions, size, folder):
    ''' creates the dataset of negative-eye-samples '''
    # work with the cropped images
    images = crop_images(images, face_loc)

    # for every image save 2 random samples
    for i in range(size):
        eye_position = scale_eye_position(eye_positions[i], face_loc[i][0])
        # get 2 negative samples
        for x in range(6):
            row, col = np.shape(images[i])
            dist_l, dist_r = (0, 0)
            # dist_l, and dist_r denote the distance to the eyeposistions
            # we want to make sure that we don't crop an real eye
            while True:
                rand_x1 = random.randint(0+25, col-25)
                rand_y1 = random.randint(0+12, row-12)
                dist_l = math.sqrt(((rand_x1-eye_position[0])**2)
                                   + ((rand_y1-eye_position[1])**2))
                dist_r = math.sqrt(((rand_x1-eye_position[2])**2)
                                   + ((rand_y1-eye_position[3])**2))
                if (dist_l > 25) and (dist_r > 25):
                    break
            # if the distance is large enough we crop the sample
            cropped = crop_bounding_box(images[i], [[rand_x1, rand_y1]])
            # safe img with ID
            misc.imsave(folder+"/Cropped_Eyes_Negative/BioID_"+str(i).zfill(4)+"_"
                        + str(rand_x1)+"_"+str(rand_y1)+".pgm", cropped[0])


def scale_eye_position(eye_position, face_loc):
    '''scale_eye_position:
                        args: eye_position = eye coordinate in original image
                              face_loc = bounding box coord. of respective face
                        output: eye positions in cropped image of the face
    '''
    xl = eye_position[0] - face_loc[3]
    yl = eye_position[1] - face_loc[0]
    xr = eye_position[2] - face_loc[3]
    yr = eye_position[3] - face_loc[0]
    return([xl, yl, xr, yr])

def create_images():
    '''Create the data from the BioID database'''
    #getting data
    filelist = load_filelist()
    images = load_Images(filelist)
    face_loc = get_face_loc(images)
    filelist_eye_positions = load_filelist(type='eye')
    eye_positions = load_eye_position(filelist_eye_positions)
    (images, face_loc, eye_positions) = remove_negatives(images,
                                                         face_loc,
                                                         eye_positions)

    Train_idx, Test_idx = train_test_split(np.arange(len(images)),
                                           test_size=0.33, random_state=42)
    X_Train = images[Train_idx]
    X_Test = images[Test_idx]
    face_loc_train = face_loc[Train_idx]
    face_loc_test = face_loc[Test_idx]
    eye_positions_test = eye_positions[Test_idx]

    # create data
    create_eye_data(X_Train, eye_positions[Train_idx], len(X_Train), 'data_final')
    create_negative_eye_data(X_Train, face_loc[Train_idx], eye_positions[Train_idx],
                             len(X_Train), 'data_final')
    create_shifted_eye_data(images[Train_idx],eye_positions[Train_idx],len(X_Train),'data_final')

    np.save('data_final/images_train.npy', X_Train)
    np.save('data_final/images_test.npy', X_Test)
    np.save('data_final/faceLoc_train.npy', face_loc_train)
    np.save('data_final/faceLoc_test.npy', face_loc_test)
    np.save('data_final/eye_positions_test.npy',eye_positions_test)

def create_numpy_files():
    '''Load the files created by create_images() and save them as numpy files for faster loading'''
    #load the data
    eye_data=load_Images(load_filelist(folder="data_final/Cropped_Eyes"))
    eye_data_shifted=load_Images(load_filelist(folder="data_final/Cropped_Eyes_Shifted"))
    eye_data_neg=load_Images(load_filelist(folder="data_final/Cropped_Eyes_Negative"))
    dataset = np.concatenate([eye_data,eye_data_shifted,eye_data_neg],axis=0)
    np.save('data_final/dataset.npy',dataset)
    labels = np.concatenate([np.ones(len(eye_data)+len(eye_data_shifted)), np.zeros(len(eye_data_neg))], axis = 0)
    np.save('data_final/labels.npy', labels)

def create_ground_truth_segmentation(images, eyepos, thresh):
    images = np.zeros(np.shape(images))
    for image, pos in zip(images, eyepos):
        cv2.ellipse(image,(pos[0], pos[1]), (15,8),0,0,360, (1), -1)
        cv2.ellipse(image,(pos[2], pos[3]), (15,8),0,0,360, (1), -1)
    images = np.array([image for image in images])
    np.save('data_final/gt_segmentation.npy', images)

if __name__ == '__main__':
    # testing the methods
    filelist = load_filelist()
    images = load_Images(filelist)
    # face_loc = get_face_loc(images)
    filelist_eye_positions = load_filelist(type='eye')
    eye_positions = load_eye_position(filelist_eye_positions)
