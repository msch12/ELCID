#!/usr/bin/env python3
#Author Michael Anders
from skimage import data
import skimage.filters as filter
from skimage.feature import peak_local_max
import numpy as np
import math
import matplotlib.pyplot as plt

def accuracy(images,eye_positions,face_loc,k=1.4,sigma=4.0,dist=20):
#Calculates the accuracy of at least one Blob being close to an Eye
    new_coords=[]
    coords=np.array(dogInterestPoints(images,k,sigma,dist))
    n=0
    #N= number of Blobs for any given image
    for coord in coords:
        n = n+ len(coord)
    for coord,face in zip(coords,face_loc):
        a=[[coord[i,1]+face[0][3],coord[i,0]+face[0][0]] for i in range(len(coord))]
        new_coords.append(np.array(a))
    imagehits=[]
    #Check distance of every Blob to the Eye Positions
    for i in range(len(images)):
        dists= [[math.sqrt((coord[0]-eye_positions[i][0])**2+(coord[1]-eye_positions[i][1])**2)
        ,math.sqrt((coord[0]-eye_positions[i][2])**2+(coord[1]-eye_positions[i][3])**2)] for coord in new_coords[i]]
        hit=0
        for dist in dists:
            if dist[0]<10 or dist[1]<10:
                hit = 1
        imagehits.append(hit)
    accuracy = imagehits.count(1)/len(images)
    #Retunr accuracy and average number of Blobs
    return accuracy,(n / len(images))

def dogInterestPoints(images, k=1.4, sigma=4.0, dist=11):
#Main Blob detection function
    # functions that return the gaussian of the image
    s1 = lambda img : filter.gaussian(img,k*sigma)
    s2 = lambda img : filter.gaussian(img,sigma)

    # calculate the DifferencesOfGradients
    dog = [s1(img)-s2(img) for img in images]

    # find the local maxima to calculate the coordinates of interesting Points
    coordinates = [peak_local_max(dog_, min_distance=dist) for dog_ in dog]
    return coordinates

if __name__ == '__main__':
#Test Blob Detection with sample image
    cropped = np.load('cropped_img.npy')
    ex_img = cropped[0:2]
    iPoints = dogInterestPoints(ex_img)

    plt.imshow(ex_img[1])
    plt.scatter(iPoints[1][:,1], iPoints[1][:,0], c='r')

    plt.show()
