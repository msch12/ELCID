#!/usr/bin/env python3
#Author Michael Anders & Max Schumacher
import torch
import matplotlib.pyplot as plt
import numpy as np
import cv2
from dataset import *
from data_preperation import *
#from CNN_eyes import CNN,train_model,test_model,predict
import scipy.misc as misc
from skimage.color import rgb2gray
import time
from FCN32 import *
from CNN_eyes import *
# import tensorflow as tf

def train_CNN(M,n_epochs=10):
    '''Train the CNN on the previously created Dataset'''
    eyes_train = EYE_DATASET('data_final/dataset.npy', 'data_final/labels.npy')
    dataloader = DataLoader(eyes_train, batch_size=32, shuffle=True)
    train_model(M, dataloader, True, n_epochs=n_epochs)

def train_FCN32():
    '''Train the FCN using a Test and Train split of the dataset'''
    M = FCN32s(2)
    X = np.load('data_final/data.npy')
    Y = np.load('data_final/gt_segmentation.npy')
    X_train, X_test, y_train, y_test = train_test_split(X, Y, test_size=0.33,
                                                        random_state=42)
    np.save('data/TrainFCNX.npy', X_train)
    np.save('data/TrainFCNY.npy', y_train)

    np.save('data/TestFCNX.npy', X_test)
    np.save('data/TestFCNY.npy', y_test)

    eyes_train = EYE_DATASET('data/TrainFCNX.npy', 'data/TrainFCNY.npy')
    dataloader = DataLoader(eyes_train, batch_size=1,
                            shuffle=True)
    train_model(M, dataloader, True)

def test_approach_CNN(M):
    '''Test the approach using the saved testsplit in the data_final folder'''
    def is_in_box(eye_pos,blob_pos):
        '''Check whether the eye position is in the bounding box of a blob'''
        if eye_pos[0] <= blob_pos[1]+25 and eye_pos[0] >= blob_pos[1]-25:
            if eye_pos[1] <= blob_pos[0]+12 and eye_pos[1] >= blob_pos[0]-12:
                return True,"L"
        elif eye_pos[2]<= blob_pos[1]+25 and eye_pos[2] >= blob_pos[1]-25:
            if eye_pos[3] <= blob_pos[1]+25 and eye_pos[3] >= blob_pos[1]-25:
                return True,"R"
        return False,"N"
    # load test images
    images_test = np.load('data_final/images_test.npy')
    # get face locations from face recognition
    faceLoc_test = np.load('data_final/faceLoc_test.npy')
    # load eye_positions
    eye_positions = np.load('data_final/eye_positions_test.npy')
    # crop the images for blob detection
    cropped_test = crop_images(images_test, faceLoc_test)
    # run the dog for blob detection with dist 11
    blobs_test = dogInterestPoints(cropped_test, dist=12, k=1.4, sigma=4.0)
    # crop out interest areas found by blob detection
    interest_areas_test = [crop_bounding_box(image, dogs)
                           for dogs, image in zip(blobs_test, cropped_test)]
    #Test result
    acc_data=[]
    classifications=0 # To count false positives
    predictions = []

    for i in range(len(interest_areas_test)):

        pred_size_l=0 # To stop the same eye being recognized more than once
        pred_size_r=0
        for j in range(len(interest_areas_test[i])):
            classification = predict(M,interest_areas_test[i][j])
            if classification==1:
                classifications+=1
            scaled_pos=scale_eye_position(eye_positions[i],faceLoc_test[i][0])
            bool,LoR=is_in_box(scaled_pos,blobs_test[i][j])
            if (bool==True and LoR=="L" and pred_size_l==len(predictions) and classification==1):
                predictions.append(1)
                pred_size_r+=1
                classifications-=1
            elif (bool==True and LoR=="R" and pred_size_r==len(predictions) and classification==1):
                predictions.append(1)
                pred_size_l+=1
                classifications-=1
            else:
                predictions.append(0)
                pred_size_l+=1
                pred_size_r+=1
        acc_data.append(predictions)
    eyes=0
    for array in acc_data:
        eyes = eyes+ array.count(1)
    precision= eyes/(eyes+classifications)
    recall= eyes/(eyes+(len(cropped_test)*2-eyes))
    print('Precision = {}%, Recall = {}%'.format(round(precision*100),round(recall*100)))
    print("Number of individual Eyes Detected: "+str(eyes)+" of "+str(2*len(cropped_test)))
    print("Number of False Positives: "+ str(classifications))

def test_approach_FCN(model, split='train'):
    '''Test the approach using the saved testsplit in the data_final folder'''
    def is_in_box(eye_pos,blob_pos):
        '''Check whether the eye position is in the bounding box of a blob'''
        if eye_pos[0] <= blob_pos[0]+25 and eye_pos[0] >= blob_pos[0]-25:
            if eye_pos[1] <= blob_pos[1]+12 and eye_pos[1] >= blob_pos[1]-12:
                return True,"L"
        elif eye_pos[2]<= blob_pos[0]+25 and eye_pos[2] >= blob_pos[0]-25:
            if eye_pos[3] <= blob_pos[1]+25 and eye_pos[3] >= blob_pos[1]-25:
                return True,"R"
        return False,"N"

    if split == 'test':
        # load test images
        images = np.load('data/TestFCNX.npy')
        # load eye_positions
        eye_positions = np.load('data/eyeposTestFCN.npy')
    else:
        # load test images
        images = np.load('data/TrainFCNX.npy')
        # load eye_positions
        eye_positions = np.load('data/eyeposTrainFCN.npy')
    # get the predictions
    model = model.cuda()
    predictions = np.array([get_prediction(model(to_cuda(x))) for x in images])
    # get the centers of possible eyes
    centers = get_centers(predictions)

    true_positives = 0
    false_negatives = 0
    false_positives = 0
    # calculate acc, recall, number of eyes detected, number of false positives
    for real_pos, est_pos in zip(eye_positions, centers):
        L = False
        R = False
        if np.shape(est_pos)[1]>0:
            bool_ = [is_in_box(real_pos, estimated)[0] for estimated in est_pos]
            LoR = [is_in_box(real_pos, estimated)[1] for estimated in est_pos]
            for b, lr in zip(bool_, LoR):
                if b == False:
                    false_positives += 1
                else:
                    if (L == False and lr == 'L'):
                        true_positives += 1
                        L = True
                    elif (R == False and lr =='R'):
                        true_positives += 1
                        R = True
        if L == False:
            false_negatives += 1
        if R == False:
            false_negatives += 1
    precision = true_positives/(true_positives+false_positives)*100
    recall = true_positives/(true_positives+false_negatives)*100

    print('precision = {}%'.format(round(precision)))
    print('recall = {}%'.format(round(recall)))

    return true_positives, false_positives, false_negatives

def run_CNN_approach(img):
    '''Run the complete CNN approach with an Image as input
    Returns a string saying whether at least one open Eye was detected or not'''
    face_loc = get_face_loc([img])
    if face_loc==None:
        return "No face could be located"
    cropped_face= crop_images([img],face_loc)[0]
    cropped_face=rgb2gray(cropped_face)
    blobs=dogInterestPoints([cropped_face])[0]
    interest_areas=crop_bounding_box(cropped_face,blobs)
    predictions=[]
    M=CNN()
    M.load_state_dict(torch.load('data_final/CNN_eyes.ckpt'))
    for area in interest_areas:
        prediction=predict(M,area)
        predictions.append(prediction)
    if predictions.count(1) > 0:
        print("Eyes detected")
        return "Eyes detected"
    else:
        print("No Eyes detected")
        return "No Eyes detected"

def time_approach():
    '''After inputing the path of an image times how long the complete classifications
    needs'''
    path=input("Please enter path to image: ")
    img=misc.imread(path)
    #timer starts
    start=time.time()
    run_CNN_approach(img)
    #timer stops
    end=time.time()
    timer= end - start
    print("Approach took "+str(int(timer*1000))+"ms to analyze.")

def video_capture_eye_detection():
    '''Perform the classification live using a webcam'''
    video_capture = cv2.VideoCapture(0)
    process_this_frame = True
    while True:
        ret,frame = video_capture.read()
        small_frame = cv2.resize(frame, (0,0),fx=0.6,fy=0.6)
        if process_this_frame:
            str=run_CNN_approach(small_frame)
        process_this_frame = not process_this_frame
        cv2.putText(frame,str,(10,30),cv2.FONT_HERSHEY_DUPLEX,1.0,(255,255,255),1)
        cv2.imshow('Video',frame)
        if cv2.waitKey(1) & 0xFF == ord('q'):
            break
    video_capture.release()
    cv2.destroyAllWindows()

def get_prediction(prediction):
    '''Args: prediction = output of FCN32
    Returns: pixelwise prediction in format of groundtruth'''
    pred1 = prediction[0][0].cpu().detach().numpy()
    pred2 = prediction[0][1].cpu().detach().numpy()
    return np.greater(pred2, pred1).astype(int)

def to_cuda(ex):
    '''Convert a given Input to Cuda FloatTensor'''
    ex = torch.FloatTensor(ex).cuda()
    return ex.view(1,1,286,384)

def eval(model, testX, testY):
    '''Evaluation of a given model on testset: testX, testY'''
    # convert Model to cuda for faster prediction
    model = model.cuda()
    predictions = np.array([get_prediction(model(to_cuda(x))) for x in testX])

    true_pos = np.array([])
    true_neg = np.array([])
    false_pos = np.array([])
    false_neg = np.array([])
    accuracy = np.array([])

    for pred, true in zip(predictions, testY):

        pred = pred.flatten()
        true = true.flatten()

        equal = np.equal(pred, true).astype(int)
        not_equal = np.not_equal(pred,true).astype(int)
        class1 = np.equal(true , 1).astype(int)
        class0 = np.equal(true , 0).astype(int)



        true_positives = np.array([1 if (equal_ and class1_ ) else 0 for equal_, class1_ in zip(equal, class1)])

        false_positives = np.array([1 if (nequal_ and class1_) else 0 for nequal_, class1_ in zip(not_equal, class1)])
        false_negatives = np.array([1 if (nequal_ and class0_) else 0 for nequal_, class0_ in zip(not_equal, class0)])

        IoU = np.sum(true_positives)/(np.sum(true_positives) +
                                                  np.sum(false_negatives) +
                                                  np.sum(false_positives))

        accuracy = np.append(accuracy, IoU)
        true_pos = np.append(true_pos, np.sum(true_positives))
        false_pos = np.append(false_pos, np.sum(false_positives))
        false_neg = np.append(false_neg, np.sum(false_negatives))

        tp = np.sum(true_pos)/(np.sum(true_pos)+np.sum(false_pos)+np.sum(false_neg))
        fp = np.sum(false_pos)/(np.sum(true_pos)+np.sum(false_pos)+np.sum(false_neg))
        fn = np.sum(false_neg)/(np.sum(true_pos)+np.sum(false_pos)+np.sum(false_neg))

    return np.mean(accuracy), tp, fp, fn

def get_BB(prediction):
    '''Segmentation like prediction'''
    misc.imsave('gray.jpg', prediction*255)
    gray = misc.imread('gray.jpg')

    mser = cv2.MSER_create()
    regions = mser.detectRegions(gray)
    boxes = [cv2.boundingRect(r) for r in regions[0]]
    if len(boxes)>1:
        boxes = mergeBoxes(boxes)
    return boxes

def get_center(prediction):
    '''Get the center coordinate of a given prediction'''
    boxes = get_BB(prediction)
    centers = np.array([])
    for box in boxes:
        (x, y, w, h) = box
        center = [x+w/2, y+h/2]
        centers = np.append(centers, center)
    return centers

def get_centers(prediction_set):
    '''Get the center coordinate of multiple predictions'''
    def get_center_instance(instance):
        boxes = get_BB(instance)
        centers = np.array([])
        for box in boxes:
            (x, y, w, h) = box
            center = np.array([x+w/2, y+h/2]).astype(int)
            if len(centers) == 1:
                centers = center
            else:
                centers = np.vstack(centers, center)
        return centers

    centers = [[get_center(prediction)] if (len(get_center(prediction)) == 2)
               else get_center(prediction).reshape(2,-1) for prediction in prediction_set]
    return centers

def mergeBoxes(boxes):
    '''Merge two prediction boxes if the are close enough together to touch'''
    # test if two given boxes touch or overlap
    def collide(box1, box2):
        return (box1[0] <= box2[0]+box2[2]
                and box2[0] <= box1[0]+box1[2]
                and box1[1] <= box2[1]+box2[3]
                and box2[1] <= box1[1]+box1[3])

    # returns a set of boxes that collide with box1
    # nearBoxes are candidates to be merged with box1
    def nearBoxes(box1):
        nBoxes = np.array([box1])
        for box2 in boxes:
            if collide(box1, box2):
                nBoxes = np.append(nBoxes, box2)
        return nBoxes.reshape(-1, 4)

    # gives an bounding box for a given set of boxes
    def findMaxBound(boxes):
        #
        # xy_mins =
        #

        xmin = boxes[0][0]
        ymin = boxes[0][1]
        xmax = boxes[0][0]+boxes[0][2]
        ymax = boxes[0][1]+boxes[0][3]

        for box in boxes:
            if box[0] < xmin:
                xmin = box[0]
            if box[1] < ymin:
                ymin = box[1]
            if box[0] + box[2] > xmax:
                xmax = box[0] + box[2]
            if box[1] + box[3] > ymax:
                ymax = box[1] + box[3]
        return (xmin, ymin, xmax-xmin, ymax-ymin)

    # array for the new merged Boxes
    mergedBoxes = np.array([])

    # for every box find the set of "nearBoxes" and then
    # merge the set to only one box for the area
    for box in boxes:
        nBoxes = nearBoxes(box)
        mergedBoxes = np.append(mergedBoxes,
                                findMaxBound(nBoxes)).reshape(-1, 4)

    # overwriting the old boxes
    # with the fewer mergedBoxes without duplicates
    boxes = np.unique(mergedBoxes.astype(int), axis=0)
    return boxes


if __name__ == '__main__':
    # M=CNN()

    #Create train and test data
    # create_images()
    # create_numpy_files()

    #Train the CNN with the train data
    # train_cnn(M,100)

    # load model from checkpoint
    # M.load_state_dict(torch.load('data_final/CNN_eyes.ckpt'))

    #Test the approach using the test dataset
    # test_approach(M)

    #Time the runtime for analyzation of one image
    #time_approach()

    #Test Eye detection using Video video_capture
    #video_capture_eye_detection()

    #Load and test the FCN Approach
    model = FCN32s(2)
    model.load_state_dict(torch.load('data_final/checkpoints2/training-120.ckpt'))
