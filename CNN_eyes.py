#!/usr/bin/env python3
#Author: Max Schumacher
import torch
import torch.nn as nn
import torch.nn.functional as F
from torch.nn.init import xavier_normal_, normal_
import numpy as np
from dataset import *
from sklearn.model_selection import train_test_split
from CNN_eyes import *
# from logger import Logger

try:
    from tqdm import tqdm, trange
    print_fn = tqdm.write
    has_tqdm = True
except ImportError:
    print_fn = print
    has_tqdm = False


def set_weights(module):
    '''
    Setting random weights and bias for a given module (normal distribution).
    '''
    if isinstance(module, nn.Conv2d):
        xavier_normal_(module.weight)
        normal_(module.bias)
    elif isinstance(module, nn.Linear):
        normal_(module.weight)
        normal_(module.bias)


class CNN(nn.Module):
    '''
    Input: image of size 50x24
    Output: 2 classes: is eye or is not an eye
    '''
    def __init__(self):
        super(CNN, self).__init__()
        self.conv1 = nn.Conv2d(1, 16, kernel_size=3, stride=1, padding=1)
        self.conv2 = nn.Conv2d(16, 32, kernel_size=3, stride=1, padding=1)
        #+self.pool = nn.MaxPool2d(kernel_size=2, stride=2)
        #self.conv3 = nn.Conv2d(32, 64, kernel_size=3, stride=1, padding=1)
        self.fc1 = nn.Linear(24*50*32, 100)
        self.fc2 = nn.Linear(100, 2)
        self.apply(set_weights)

    def forward(self, x):
        x = F.relu(self.conv1(x))
        x = F.relu(self.conv2(x))
        #x = self.pool(x)
        #x = F.relu(self.conv3(x))
        x = x.reshape(x.size(0), -1)
        x = F.relu(self.fc1(x))
        return self.fc2(x)

def predict(M,img):
    img_ = convert_to_FloatTensor(img)
    prediction = int(torch.argmax(M(img_)))
    return prediction


def train_model(model, dataloader, use_gpu, n_epochs=20):
#Used to train the CNN with the given trainset
    logger = Logger('./logs')

    if use_gpu:
        model.cuda()

    # loss and optimizer
    Loss = nn.CrossEntropyLoss()
    Optimizer = torch.optim.Adam(model.parameters())
    Optimizer.zero_grad()

    if has_tqdm:
        e_iterator = trange(n_epochs, desc='epoch')
    else:
        e_iterator = range(n_epochs)

    for e in e_iterator:
        if has_tqdm:
            iterator = enumerate(tqdm(dataloader, desc='batch'))
        else:
            iterator = enumerate(dataloader)

        for e_step, (x, y) in iterator:
            train_step = e_step + len(dataloader)*e

            if use_gpu:
                x = x.cuda()
                y = y.cuda()

            x = x.view((32, 1, 24, 50))

            # Forward
            prediction = model(x)

            loss = Loss(prediction, y)
            acc = torch.mean(torch.eq(torch.argmax(prediction, dim=-1),
                                      y).float())

            Optimizer.zero_grad()

            # Backward
            loss.backward()

            # Update
            Optimizer.step()

            _,argmax= torch.max(model(x),1)
            accuracy=(y== argmax.squeeze()).float().mean()

            if train_step % 25 == 0:
                print_fn('{}: Batch-Accuracy = {}, Loss = {}'
                         .format(train_step, float(acc), float(loss)))

                #Tensorboard Logging
                info={'loss':loss.item(),'accuracy':accuracy.item()}
                for tag,value in info.items():
                    logger.scalar_summary(tag,value,e+1)
                for tag,value in model.named_parameters():
                    tag= tag.replace('.','/')
                    logger.histo_summary(tag, value.data.cpu().numpy(),e+1)
                    logger.histo_summary(tag+'/grad',value.grad.data.cpu().numpy(),e+1)

                info={'images': x.view(-1,24,50)[:10].cpu().numpy()}
                for tag,images in info.items():
                    logger.image_summary(tag,images,e+1)
    #Save the model at the end of training
    torch.save(model.state_dict(), 'data_final/CNN_eyes.ckpt')


def test_model(model, dataloader, use_gpu, n_samples):
#Test a given CNN Model
    accuracy = np.array([])
    if has_tqdm:
        iterator = enumerate(tqdm(dataloader, desc='batch'))
    else:
        iterator = enumerate(dataloader)

    for e_step, (x, y) in iterator:
        x = x.cuda()
        y = y.cuda()

        # batch_size = number of samples
        x = x.view((n_samples, 1, 24, 50))

        # Forward
        prediction = model(x)

        acc = torch.mean(torch.eq(torch.argmax(prediction, dim=-1),
                                  y).float())
        accuracy = np.append(accuracy, acc)

    return np.mean(accuracy)

def convert_to_FloatTensor(img):
#Convert an image to a FloatTensor
    img_tensor= torch.FloatTensor(img)
    img_tensor= img_tensor.view(1,1,24,50)
    return img_tensor

if __name__ == '__main__':
#Train and test a model give a dataset and the appropriate labels
    X = np.load('data_final/dataset.npy')
    Y = np.load('data_final/labels.npy')
    X_train, X_test, y_train, y_test = train_test_split(X, Y, test_size=0.33,
                                                        random_state=42)
    np.save('data/TrainX.npy', X_train)
    np.save('data/TrainY.npy', y_train)

    np.save('data/TestX.npy', X_test)
    np.save('data/TestY.npy', y_test)
    Model = CNN()
    eyes_train = EYE_DATASET('data/TrainX.npy', 'data/TrainY.npy')
    dataloader = DataLoader(eyes_train, batch_size=32,
                            shuffle=True)
    train_model(Model, dataloader, True, n_epochs=20)

    # test the model on the data
    eyes_test = EYE_DATASET('data/TestX.npy', 'data/TestY.npy')
    dataloader = DataLoader(eyes_train, batch_size=32,
                            shuffle=False)
    acc = test_model(Model, dataloader, True, 32)
    print(acc)
