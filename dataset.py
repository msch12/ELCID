#!/usr/bin/env python3
#Author Max Schumacher
from torch.utils.data import Dataset, DataLoader
import numpy as np
import torch


class EYE_DATASET(Dataset):
    '''Dataset for eye Data'''
    def __init__(self, npy_file_x, npy_file_y, transform=None):
        '''
        Arguments: npy_file: numpy array containing the data
                   root_dir: directory of the numpy file
                   transform: optional transformation on the data
        '''
        self.use_gpu = True

        X = np.load(npy_file_x)
        X = X[:len(X)-len(X)%32]
        Y = np.load(npy_file_y)[:len(X)]
        self.X = torch.FloatTensor(X)
        self.Y = torch.LongTensor(Y)
        self.transform = transform

    def __len__(self):
        return len(self.X)

    def __getitem__(self, idx):
        sample = [self.X[idx], self.Y[idx]]

        # optional transformation
        if self.transform:
            sample = self.transform(sample)

        return sample


if __name__ == '__main__':
    eyes = EYE_DATASET('data/Cropped_Eyes_Array.npy')
    img = eyes[5]
    dataloader = DataLoader(eyes, batch_size=1,
                            shuffle=True, num_workers=8)
    dset = enumerate(dataloader)
    for e_step, (x) in dset:
        print(e_step, '\n', (x))
