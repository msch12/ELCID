\Chapter{Image processing with FCN}{Max Schumacher}

  \section{Architecture}
  Our implementation is based on the \textit{FCN32}-structure. Due to the simplicity
  of our problem and limited hardware resources we simplified the network.
  This resulted in $5$ convolutional layers followed by $2$ fully convolutional layers.
  Each convolutional layer uses the ReLU activation function and is downsampled with a
  maxpool layer with a kernel-size of $2$. The score of the fully convolutional layer
  is upsampled to the input dimension by a single transposed convolutional layer
  with a stride of $32$ (\textit{FCN32}-structure). While the high stride value
  limits the scale of detail in our output the network gives sufficient accuracy
  for localizing eyes since we are not actually interested in the exact shape of
  an detected eye.

  \section{Training and post-processing}
  For the training and testset we randomly split the data and ground truth as well
  as the eyepositions with a ratio of $2:1$.
  The Network is then trained for $120$ epochs with a batch size of $1$. The output
  has the dimensions $h\times w \times 2$ where each $h\times w$ array denotes the
  propability for a pixel being either background or part of the eye with $h$ being
  the height of the original image and $w$ being the width of the original image.
  Pixelwise maxima conclude the outcome of the prediction.
  % figure of example input and belonging prediction
  \begin{figure}[!hbp]
     \centering
     \begin{minipage}[b]{0.4\textwidth}
       \includegraphics[width=\textwidth]{images/example.png}
       \caption{\textbf{Input}}
     \end{minipage}
     \hfill
     \begin{minipage}[b]{0.4\textwidth}
       \includegraphics[width=\textwidth]{images/example_prediction.png}
       \caption{\textbf{Pixelwise prediction}}
     \end{minipage}
   \end{figure}\\
   \\
   The following post-processing is divided into: $(1)$ finding maximal extremal
   regions with MSER-algorithm, $(2)$ fitting bounding boxes and getting predicted
   eye locations.\\
   \\
   After applying the MSER algorithm on the prediction of the FCN the \textit{extremal regions}
   are fitted with bounding rectangles with intersecting boxes being merged.
   This is done to avoid more than one box per maxima in a given image. For the
   eye locations we chose to take the center of the so found boxes.
   \begin{figure}[!hbp]
      \centering
      \begin{minipage}[b]{0.4\textwidth}
        \includegraphics[width=\textwidth]{images/exampleBB_1.png}
        \caption{\textbf{Bounding boxes pred.}}
      \end{minipage}
      \hfill
      \begin{minipage}[b]{0.4\textwidth}
        \includegraphics[width=\textwidth]{images/exampleBB_2.png}
        \caption{\textbf{Bounding boxes pred.}}
      \end{minipage}
    \end{figure}\\

   \section{Testing}
   We tested our implementation of the FCN on trainset and testset. To get a measurement
   of the segmentation accuracy we tested the FCN in regard to pixel accuracy
   and mean Intersect over Union(IoU). \\
   \\
   The IoU is calculated by dividing the area of Overlap by the Area of Union.
   Mathematically this can be expressed by the Jaccard-Coefficient: $$J_i(A_i,B_i)=\frac{A_i\cup B_i}{A_i \cap B_i}$$
   where $A_i$ is the surface of class $i$ and $B_i$ is the respective surface in the prediction.
   For our two class problem we measured the accuracy by calculating the mean over the Jaccard-Coefficients $\overline{IoU} =  \sum J_{1,j}/nr_{images}$
   where $J_{1,j}$ is the Jaccard-Coefficient for class $1$ for image $j$ in the testset.\\
   \\
   We also calculated the raw pixel accuracy by dividing the number of correctly classified
   pixels by the total number of pixels in any given image from the tested dataset.
   Let $n_i$ be the number of correctly predicted pixels for image $i$ and $t_i$ be
   the total number of pixels in the image. We compute the mean raw pixel accuracy through
   $$\overline{acc}_{raw}=\sum\limits_{i}\frac{n_i}{t_i}\quad with\ i \in \{{1,...,nr_{images}\}}$$
   We will use the intersect over union and the raw pixel accuracy as main indicators in regard to performance
   of our FCN.\\
   \\
   However for the full approach from input image to bounding boxes or respective eyepositions
   we need a different type of measurement for accuracy. We consider an eye to be
   recognized or accurately localized if a bounding box found by the MSER algorithm
   contains at least $80\%$ of the eye. This can be similarly expressed by a distance
   in which a proposed location is accepted. Given a fixed radius $R$, the proposed eye location
   $x$ and the real locations $y$, the accepted distance $max_{dist}$ needs to be smaller than $R$:
   $$max_{dist} = \norm{x-y}_2<R$$
   We compared every proposed eyeposition to both left and right eye-location and
   categorized the proposal as false positive if either the $max_{dist}<R$ condition
   was not satisfied or if the eyeposition has already been descriped by one of the
   other proposals. False negatives are given by the number of eyes which have not
   been recognized by any eyeposition proposal. We then calculate precision and recall
   over the full train- and test-dataset for perfomance measurement of the full approach.
   For evaluation and discussion of the perfomance of the FCN and the eye localization reffer to
   \autoref{chap:ResultsAndEvaluation} -\textit{Results and Evaluation}.
