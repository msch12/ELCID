\Chapter{CNN with region proposal based on Blob-Detection}{Michael Anders}

\section{Overview}
Our first Approach, on which we mainly focused during this project, was to introduce a form of open eye
detection which would deliver primarily fast classification to allow for multiple image
processes per second. This focus was driven by the need for fast reaction in our application
scenario, as only an algorithm that would perform the detection quickly would be suitable for micro
sleep detection. To achieve this goal we aimed at introducing a combination of image processing
techniques to reduce the size of the input images in each step thus creating a pipeline which would
work with ever decreasing image sizes. \\
\\
As a first step we perform face detection on our input image. The outputs of which are the
coordinates of the detected face in the image. With this face location we perform a crop of the
original input image to remove excessive information and focus only on the relevant parts. The
resulting face crop is then processed with a blob detection algorithm, extracting facial areas of
interest, among which the eyes stand out. These interest areas defined by bounding boxes around
the found blobs are entered into a CNN for classification. The CNN then determines whether an
interest area does in fact represent an eye or not. \\
\\
This pipeline structure would allow us to enter only small crops of the whole image into the CNN
thus reducing complicated computations and keeping the runtime low. It also resulted in a simpler
CNN which made training easier and less computationally costly. The data required for training was
extracted from the BioID dataset in the form of positive and negative eye samples. \\
\\
Figure 3.1 displays the general image processing steps of this CNN based approach. The Image Crops that result from the processing are fed into the CNN for Classification.
\begin{figure}[H]
\begin{center}
\includegraphics[scale=0.7]{images/imageproc.png}
\caption{\textbf{CNN Approach Image Processing Steps}}
\end{center}
\end{figure}

\section{Detailed Description}
Our first focus during implementation of the approach was to research and introduce a reliable face
detection algorithm. This was crucial for the success of our approach as only those faces that were
actually detected by the face localization could be processed further. Missing faces often would
result in severly reduced accuracy of our approach. As such we first tried using an OpenCV face
detection algorithm using Haar-cascade. However we soon learned that at least for our image
dataset, the accuracy of this approach was severely lacking. Of the total of 1518 pictures contained in
BioID only around 1300 were actually detected. This resulting accuracy of $85.6\%$ was not
sufficient for us as it would not allow us to properly evaluate the approach we aimed to introduce by
already filtering out a lot of the images in the first step. As a consequence we switched to a different
open source face detection algorithm [\ref{2}]. This approach uses dlib's [\ref{3}] state of the art face recognition built.
The model claims to have an accuracy of 99.38\% on the Labeled Faces in the Wild benchmark [\ref{4}].
Using this method on our dataset resulted in a similar accuracy. Of the 1518 pictures we were able to
detect a face in all but 12 of them, resulting in an accuracy of 99,2\%. \\
\\
Figure 3.2 shows the boundaries of a detected face found by the used face detection, next to the
original image. The face detection itself only outputs the face coordinates in (top,right,bottom,left)
form. So in order to further process the image we wrote our own crop function which output the
image seen on the right of Figure 3.2. \\
\\
\begin{figure}[!hbp]
\begin{center}
\includegraphics[scale=0.5]{images/facetocrop.png}
\caption{\textbf{Face Detection Example}}
\end{center}
\end{figure}
With this extracted face we moved on to the blob detection. Our initial idea was to build an iris
filter, which we would then convolve over the image, resulting in possible eye-areas which would be
passed on to the CNN. However, given the significant differences in eye shape, form, size and
general distance of the person to the camera, there were no promising results to be achieved by this.
So we rather changed our interpretation of the Iris Filter into a convolution with a Difference of
Gaussian (DoG) Kernel which we convolved over the face image. The result of this convolution can
be seen in Figure 3.3. \\
\\
\begin{figure}[!hbp]
\begin{center}
\includegraphics[scale=1]{images/convolutionresult.png}
\caption{\textbf{Result of Convolution with DoG}}
\end{center}
\end{figure}
As can be seen interesting areas are highlighted brighter in the resulting image. Such interest areas
include the eyes, the nose, the mouth, the eyebrows as well as some areas around the outline of the head. \\
\\
The Difference of Gaussian kernel has the following form: \\
$$ DoG = G_{\sigma 1} - G_{\sigma 2} $$ \\
$$ G(\sigma , x,y) = \frac{1}{\sqrt{2\pi \sigma ^2}}exp(-\frac{x^2+y^2}{2\sigma ^2})$$ \\
\\
By using the peak\textunderscore local\textunderscore max function included in the skimage library we were able to tune the
minimal distance used to determine the local maxima and thus not only receive our interest areas
but also determine how sensitive the algorithm should be, i.e. how many blobs we wished to detect.
A lower minimal distance would result in a higher number of blobs being detected as it would
perform as a sort of threshold.\\
\\
The blobs we received now had to be cropped out of
the image in a sufficiently big but not too big bounding box, as a bounding box with too much
information could result in a wrong classification through the CNN. To determine the adequate size
for these bounding boxes we manually analyzed around 30 randomly chosen pictures from the
dataset and thus came to the conclusion that a bounding box of 50x24 pixels would be adequate.
This size would proof to be precise later on when inspecting the created crops of the whole dataset.\\
\\
Now that we were able to extract the interest areas, the next step was to create the data needed to
train and test a CNN and to implement the neural network itself. We created to positive eye dataset
by using our crop methods implemented in the previous steps as well as the eye positions given in text
file form for each BioID image. We thus cropped out both eyes with a 50x24 bounding box, having
the eye position directly at the center. Then to introduce some more randomized data, as our blob
detection would not always have interest areas directly at the center of the eye, we also cropped out
each eye again but shifted by a random factor in both X and Y direction in such a way that the eye
would still be fully depicted on the crop. Our negative eye data now had to be equally as big in
number of pictures in order to achieve a 50:50 balance. In order to achieve this we chose multiple
random coordinates for each picture, which were far enough in distance, so as not to include an eye,
and cropped out a bounding box around those coordinates. \\
\\
Having created all required data we now took to the task of creating the CNN itself. For this we
used the torch library to write a neural network which would take an input image with the
dimensions of 50x24 and output the likelihood of that image either containing an eye or not
containing one. The network itself contains two convolutional and two fully connected layers
and is thus kept rather simple. For the activation function we used ReLU and Adam as the Optimizer for our network. To meassure the performance of our classification model we used Cross-entropy loss. \\
\\
The CNN was then trained on a train data split which we created beforehand from two thirds of both
the positive and negative eye crops. The rest of the split was formed into a test split for later
evaluation of the CNN. The training of the CNN was not very costly in terms of processing time,
because of its simplicity. We worked with different numbers of epochs for training in order to find
an appropriate cut of point. As can be seen below, the loss of each epoch was tracked using
Tensorflow and does not significantly change after about 50 epochs. As seen in Figure 3.5 however
batch accuracy outliers are reduced significantly with some further epochs until about 75 epochs. In
the end we settled on the selection of the model using 100 epochs to have both loss and batch
accuracy at a favorable position. \\

\begin{figure}[!hbp]
\begin{center}
\includegraphics[scale=0.35]{images/loss.png}
\caption{\textbf{CNN loss over 100 epochs}}
\end{center}
\end{figure}
\begin{figure}[!hbp]
\begin{center}
\includegraphics[scale=0.35]{images/accuracy.png}
\caption{\textbf{CNN accuracy over 100 epochs}}
\end{center}
\end{figure}

With our model complete all that was left to do was clean up the code and combine all the different
techniques into one in order to complete the approach. This then allowed us to move on to tuning of
the different parameters and then evaluate the approach.
