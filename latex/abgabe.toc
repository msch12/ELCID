\contentsline {chapter}{\numberline {1}Introduction: {\textnormal {Michael Anders}}}{4}{chapter.1}
\contentsline {chapter}{\numberline {2}Theoretical Background: {\textnormal {Max Schumacher}}}{7}{chapter.2}
\contentsline {section}{\numberline {2.1}MSER-Maximally Stable Extremal Regions}{7}{section.2.1}
\contentsline {section}{\numberline {2.2}Convolutional Neural Networks}{7}{section.2.2}
\contentsline {chapter}{\numberline {3}CNN with region proposal based on Blob-Detection: {\textnormal {Michael Anders}}}{9}{chapter.3}
\contentsline {section}{\numberline {3.1}Overview}{9}{section.3.1}
\contentsline {section}{\numberline {3.2}Detailed Description}{10}{section.3.2}
\contentsline {chapter}{\numberline {4}Image processing with FCN: {\textnormal {Max Schumacher}}}{14}{chapter.4}
\contentsline {section}{\numberline {4.1}Architecture}{14}{section.4.1}
\contentsline {section}{\numberline {4.2}Training and post-processing}{14}{section.4.2}
\contentsline {section}{\numberline {4.3}Testing}{15}{section.4.3}
\contentsline {chapter}{\numberline {5}Results and Evaluation}{17}{chapter.5}
\contentsline {section}{\numberline {5.1}Results CNN Approach: {\textnormal {Michael Anders}}}{17}{section.5.1}
\contentsline {section}{\numberline {5.2}Evaluation CNN Approach: {\textnormal {Michael Anders}}}{18}{section.5.2}
\contentsline {section}{\numberline {5.3}Results of the FCN Approach: {\textnormal {Max Schumacher}}}{19}{section.5.3}
\contentsline {subsection}{\numberline {5.3.1}Benchmarking the FCN Approach}{19}{subsection.5.3.1}
\contentsline {section}{\numberline {5.4}Evaluation of the FCN approach}{21}{section.5.4}
\contentsline {chapter}{\numberline {6}Conclusion}{22}{chapter.6}
\contentsline {section}{\numberline {6.1}Summary: {\textnormal {Michael Anders}}}{22}{section.6.1}
\contentsline {chapter}{References}{23}{section.6.1}
